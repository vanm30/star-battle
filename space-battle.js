$(document).ready(function () {

//Variables
    var enemyHP;
    var myHP;
    var enemyHPShow = $('#top-content');
    var myHPShow = $('#bottom-content');
    var mapWidth = $('#map').width();
    var invaderSpeed;
    var redLaserSpeed;
    var time = 0;
    var lastDifficulty;

//Intervals
    var intervalGreenLaser;
    var intervalRedLaser;
    var timer;
    var moveInvader;
    var randomShoot;


//Initialization functions
    mainMenu();


//View Main Menu
    function mainMenu() {
        clearGame();

        $('#map').append("<br/>");
        $('#map').append("<br/>");
        $('#map').append("<br/>");
        $('#map').append("<br/>");
        $('#map').append("<p>Welcome to Star Battle" + "<br />" + "<br />" + "Choose Difficulty</p>");

        var buttonEasy = $('<button/>',
            {
                text: 'Easy',
                click: function () {
                    startGame(1)
                }
            });
        var buttonNormal = $('<button/>',
            {
                text: 'Normal',
                click: function () {
                    startGame(2)
                }
            });
        var buttonImpossible = $('<button/>',
            {
                text: 'Impossible',
                click: function () {
                    startGame(3)
                }
            });


        $('#map').append(buttonEasy);
        $('#map').append("<br/>");
        $('#map').append(buttonNormal);
        $('#map').append("<br>");
        $('#map').append(buttonImpossible);

    }


//Start Game
// n = game mode
    function startGame(n) {
        clearGame();
        $('#map').append('<img id="ship" src="/src/ship.png"  alt="Ship not showing"/>');
        $('#map').append('<img id="invader" src="/src/invader.png"  alt="Invader not showing"/>');
        $("#invader").css({
            top: randomNumberFromTo(0, 35) + '%',
            left: randomNumberFromTo(0, 90) + '%',
            position: 'absolute'
        });
        switch (n) {
            case 1:
                enemyHP = 1;
                myHP = 5;
                invaderSpeed = randomNumberFromTo(600, 700);
                redLaserSpeed = randomNumberFromTo(50, 60);
                lastDifficulty = 1;
                break;
            case 2:
                enemyHP = 6;
                myHP = 2;
                invaderSpeed = randomNumberFromTo(400, 500);
                redLaserSpeed = randomNumberFromTo(25, 30);
                lastDifficulty = 2;
                break;
            case 3:
                enemyHP = 8;
                myHP = 1;
                invaderSpeed = randomNumberFromTo(200, 300);
                redLaserSpeed = randomNumberFromTo(20, 25);
                lastDifficulty = 3;
                break;
        }
        enemyHPShow.val("LIVES: " + enemyHP);
        myHPShow.val("LIVES: " + myHP);
        time = 0;
        startTimer();

        //Red laser init
        randomShoot = setInterval(function () {
            if ($('#redLaser').length === 0) {
                var currentInvaderPosLeft = $('#invader').offset().left;
                var currentInvaderPosTop = $('#invader').offset().top;
                $('#map').append('<img id="redLaser" src="/src/redLaser.png"  alt="Laser not showing"/>');
                $("#redLaser").offset({top: currentInvaderPosTop + 50, left: currentInvaderPosLeft + 25});
                moveRedLaser();
            }
        }, 2000);

        //Invader movement
        moveInvader = setInterval(function () {
            $('#invader').animate({
                    top: randomNumberFromTo(0, 35) + '%',
                    left: randomNumberFromTo(0, 90) + '%'
                },
                randomNumberFromTo(600, 800));
        }, invaderSpeed);
    }

    function startTimer() {
        timer = window.setInterval(function () {
            time = time + 0.1;
        }, 100);
    }


//Ship movement
    $(document).keydown(function (e) {
        if ($("#ship").length !== 0) {
            var position = $("#ship").position().left;
            switch (e.keyCode) {
                case 68, 39: //RIGHT
                    if (position + 50 < mapWidth) {
                        $("#ship").animate({left: "+=50"}, 50);
                    }
                    break;
                case 65, 37: //LEFT
                    if (position > 0) {
                        $("#ship").animate({left: "-=50"}, 50);
                    }
                    break;
            }
        }

    });

//Green laser init
    $(document).keydown(function (e) {
        if ($("#ship").length !== 0) {
            if (e.keyCode === 32) {
                if ($('#laser').length === 0) {
                    var currentShipPosLeft = $('#ship').offset().left;
                    var currentShipPosTop = $('#ship').offset().top;
                    $('#map').append('<img id="laser" src="/src/greenLaser.png"  alt="Laser not showing"/>');
                    $("#laser").offset({top: currentShipPosTop - 30, left: currentShipPosLeft + 22});
                    moveGreenLaser();
                }
            }
        }
    });


//Green laser path
    function moveGreenLaser() {
        intervalGreenLaser = setInterval(function () {
            $("#laser").animate({top: "-=20"}, 30);
            checkColisionGreen();
        }, 30);
    }

//Red laser path
    function moveRedLaser() {
        intervalRedLaser = setInterval(function () {
            $("#redLaser").animate({top: "+=20"}, redLaserSpeed);
            checkColisionRed();
        }, redLaserSpeed);
    }

//Green laser collision check
    function checkColisionGreen() {
        if (collision($('#laser'), $('#invader'))) {
            $('#laser').remove();
            enemyHP--;
            enemyHPShow.css('color', 'red');
            setTimeout(function (){
                enemyHPShow.css('color', 'white');
            }, 500);
            enemyHPShow.val("LIVES: " + enemyHP);
            if (enemyHP === 0) {
                gameWin();
            }
            clearInterval(intervalGreenLaser);
        } else if (collision($('#laser'), $('#top-border'))) {
            $('#laser').remove();
            clearInterval(intervalGreenLaser);
        }

    }

//Red laser collision check
    function checkColisionRed() {
        if (collision($('#redLaser'), $('#ship'))) {
            $('#redLaser').remove();
            clearInterval(intervalRedLaser);
            myHP--;
            myHPShow.css('color', 'red');
            setTimeout(function (){
                myHPShow.css('color', 'white');
            }, 500);
            myHPShow.val("LIVES: " + myHP);
            if (myHP === 0) {
                gameOver();
            }
        } else if (collision($('#redLaser'), $('#bottom-border'))) {
            $('#redLaser').remove();
            clearInterval(intervalRedLaser);
        }
    }


    function gameOver() {
        clearGame();
        clearInterval(intervalRedLaser);
        clearInterval(randomShoot);
        clearInterval(intervalGreenLaser);
        clearInterval(moveInvader);
        clearInterval(timer);

        $('#map').append("<div class='text-area'> " +
            "<p>Game Over!</p> " +
            "<p>Wanna play again?</p>" +
            "</div>");

        var buttonMenu = $('<button/>',
            {
                text: 'Menu',
                click: function () {
                    mainMenu()
                }
            });
        var buttonPlayAgain = $('<button/>',
            {
                text: 'Play Again',
                click: function () {
                    startGame(lastDifficulty)
                }
            });


        $('#map').append(buttonPlayAgain);
        $('#map').append("<br/>");
        $('#map').append(buttonMenu);
    }

    function gameWin() {
        $('#map').empty();
        clearInterval(randomShoot);
        clearInterval(intervalGreenLaser);
        clearInterval(intervalRedLaser);
        clearInterval(moveInvader);
        clearInterval(timer);

        $('#map').append("<div class='text-area'> " +
            "<p>You Win!</p> " +
            "<p>Your time was " +
            time.toFixed(2) + " seconds." +
            "</p>" +
            "<p>Wanna play again?</p>" +
            "</div>");

        var buttonMenu = $('<button/>',
            {
                text: 'Menu',
                click: function () {
                    mainMenu()
                }
            });
        var buttonPlayAgain = $('<button/>',
            {
                text: 'Play Again',
                click: function () {
                    startGame(lastDifficulty)
                }
            });


        $('#map').append(buttonPlayAgain);
        $('#map').append("<br/>");
        $('#map').append(buttonMenu);

        if ((localStorage.getItem(lastDifficulty) == null) || (localStorage.getItem(lastDifficulty) > time.toFixed(2))) {
            localStorage.setItem(lastDifficulty, time.toFixed(2));
        }
    }


//SUPPORTIVE FUN


    function clearGame() {
        $('#map').empty();
        $('#top-content').empty();
        $('#bottom-content').empty();
    }

//random number generater bettween min and max
    function randomNumberFromTo(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

//Collision check between two divs
    function collision($div1, $div2) {
        var x1 = $div1.offset().left;
        var y1 = $div1.offset().top;
        var h1 = $div1.outerHeight(true);
        var w1 = $div1.outerWidth(true);
        var b1 = y1 + h1;
        var r1 = x1 + w1;
        var x2 = $div2.offset().left;
        var y2 = $div2.offset().top;
        var h2 = $div2.outerHeight(true);
        var w2 = $div2.outerWidth(true);
        var b2 = y2 + h2;
        var r2 = x2 + w2;

        if (b1 < y2 || y1 > b2 || r1 < x2 || x1 > r2) return false;
        return true;
    }

//Spacebar in not scrolling page
    window.addEventListener('keydown', (e) => {
        if ((e.keyCode === 32 || e.keyCode === 38 || e.keyCode === 40) && e.target === document.body) {
            e.preventDefault();
        }
    });

});





