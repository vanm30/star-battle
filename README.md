#Star Battle

A simple web-browser game based on some famous arcade games.

## Technologies
Project is created with:
* HTML
* CSS
* JavaScript
* Jquery-3.1.1

##Author

Matěj Vaník, student at VŠE-FIS