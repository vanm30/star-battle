$(document).ready(function () {

//Change active nav button
    $('ul li').click(function () {
        $(this).addClass('active');
        $(this).parent().children('li').not(this).removeClass('active');
    });

//Buttons of nav
    $('#button_about').click(function () {
        window.location.href = 'about.html';
    })

    $('#button_game').click(function () {
        window.location.href = 'index.html';
    });

    $('#button_score').click(function () {
        $('.main').empty();
        $('.main').append('<div class="score"></div>');

        var easyScore = localStorage.getItem("1");
        var normalScore = localStorage.getItem("2");
        var impossibleScore = localStorage.getItem("3");

        $('.score').append("<p>HIGHEST SCORE IN EACH GAME MODES</p>");

        if (easyScore !== null) {
            $('.score').append("<p>Easy mode: " + easyScore + " s</p>");
        } else {
            $('.score').append("<p>Easy mode: No record yet.</p>");
        }

        if (normalScore !== null) {
            $('.score').append("<p>Normal mode: " + normalScore + " s</p>");
        } else {
            $('.score').append("<p>Normal mode: No record yet.</p>");
        }
        if (impossibleScore !== null) {
            $('.score').append("<p>Impossible mode: " + impossibleScore + " s</p>");
        } else {
            $('.score').append("<p>Impossible mode: No record yet.</p>");
        }
    });
});